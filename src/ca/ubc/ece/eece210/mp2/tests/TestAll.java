package ca.ubc.ece.eece210.mp2.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.io.*;
import java.util.Scanner;

import ca.ubc.ece.eece210.mp2.Album;
import ca.ubc.ece.eece210.mp2.Genre;
import ca.ubc.ece.eece210.mp2.Catalogue;

/* Required test cases:
 * 1. Add an Album to a Genre
 * 2. Remove an Album from a Genre
 * 3. Save an Album to the String form
 * 4. Recreate an Album from the String form
 * 5. Save a Genre to the String from
 * 6. Recreate the Genre from the String form
 * 7. Save the whole catalogue to a file - SEE TestSavingCatalogue.java
 * 8. Recreate the catalogue from a file - SEE TestRebuildCatalogue.java
 * 9. Write a test to verify the Genre inclusion rules
 */

public class TestAll {
	static Catalogue catalogue;
	static Album testalbum;
	static Genre testgenre;
	
    @BeforeClass
    public static void setUpClass() throws Exception {
    	catalogue = new Catalogue();
    	testgenre = new Genre("Test Genre");
		ArrayList<String> songs = new ArrayList<String>();
		songs.add("Never Gonna Give You Up");
		songs.add("Whenever You Need Somebody");
		songs.add("Together Forever");
		testalbum = new Album("Best Hits","Rick Astley",songs);
    }
	
	@Before
	public void setUp() throws Exception {
		
	}
	
	/* Test 1. Add an Album to a Genre */
	@Test
	public void testAlbumAdd() {
		testgenre.addToGenre(testalbum);
		assert(testgenre.getChildren().contains(testalbum));
	}
	
	/* Test 2. Remove an Album from a Genre */
	@Test
	public void testAlbumRemove() {
		testgenre.addToGenre(testalbum);
		testalbum.removeFromGenre();
		assert(!testgenre.getChildren().contains(testalbum));
	}
	
	/* Test 3. Save an Album to the String form */
	@Test
	public void testSaveAlbum() {
		String str = "\"album\":\"{\"title\":\"Best_Hits\",\"performer\":\"Rick_Astley\",\"songlist\":{0:\"Never_Gonna_Give_You_Up\",1:\"Whenever_You_Need_Somebody\",2:\"Together_Forever\"}} ";
		assertEquals(testalbum.toString(),str);
	}
	
	/* Test 4. Recreate an Album from the String form */
	@Test
	public void testRestoreAlbum() {
		String str = "\"album\":\"{\"title\":\"Best_Hits\",\"performer\":\"Rick_Astley\",\"songlist\":{0:\"Never_Gonna_Give_You_Up\",1:\"Whenever_You_Need_Somebody\",2:\"Together_Forever\"}} ";
		Album restore = new Album(str);
		System.out.println(restore.toString());
		assertEquals(restore.toString(),str);
	}
	
	/* Test 5. Save a Genre to the String from */
	@Test
	public void testSaveGenre() {
		testgenre.addToGenre(testalbum);
		assertEquals(testgenre.toString()," {\"genre\":\"Test_Genre\" < \"album\":\"{\"title\":\"Best_Hits\",\"performer\":\"Rick_Astley\",\"songlist\":{0:\"Never_Gonna_Give_You_Up\",1:\"Whenever_You_Need_Somebody\",2:\"Together_Forever\"}}  > \\End\\");
	}
	/* Test 6. Recreate the Genre from the String form */
	@Test
	public void testRestoreGenre() {
		Genre restore = Genre.restoreCollection(" {\"genre\":\"Restored_Genre\" < \"album\":\"{\"title\":\"Best_Hits\",\"performer\":\"Rick_Astley\",\"songlist\":{0:\"Never_Gonna_Give_You_Up\",1:\"Whenever_You_Need_Somebody\",2:\"Together_Forever\"}}  > \\End\\");
		System.out.println(restore.toString());
		assertEquals(restore.toString()," {\"genre\":\"Restored_Genre\" < \"album\":\"{\"title\":\"Best_Hits\",\"performer\":\"Rick_Astley\",\"songlist\":{0:\"Never_Gonna_Give_You_Up\",1:\"Whenever_You_Need_Somebody\",2:\"Together_Forever\"}}  > \\End\\");
	}
		
	/* Test 9: Genre Rules
	 * 1. Genre can have only one parent.
	 * 2. If an album is contained in a genre, it cannot be contained in any of the sub-genres.
	 * 3. An album can only belong to one genre.
	 */
	@Test
	public void testGenreRules1() {
		Genre genre1 = new Genre("Good Stuff");
		Genre genre2 = new Genre("Sub Genre");
		Genre genre3 = new Genre("Third Genre");
		genre1.addToGenre(genre2);
		genre1.addToGenre(genre3);
		genre3.addToGenre(genre2);
		/*
		 * At this point, it should look like this:
		 * 		genre1
		 * 		  |
		 * 		genre2
		 * 		  |
		 * 		genre3
		 */
		assert(!genre1.getChildren().contains(genre3));
	}
	
	@Test
	public void testGenreRules2() {
		Genre genre4 = new Genre("Genre4");
		Genre genre5 = new Genre("Genre5");
		Album greatest = new Album( "Greatest Hits", "Daler Mehndi", new ArrayList<String>(Arrays.asList( "Tunak","Tunak","Tun" )));
		genre4.addToGenre(genre5);
		genre4.addToGenre(greatest);
		assert(!genre5.getChildren().contains(greatest));
	}
	
	@Test
	public void testGenreRule3() {
		Genre genre6 = new Genre("Genre6");
		Genre genre7 = new Genre("Genre7");
		Album greatest = new Album( "Greatest Hits", "Daler Mehndi", new ArrayList<String>(Arrays.asList( "Tunak","Tunak","Tun" )));
		genre7.addToGenre(greatest);
		genre6.addToGenre(greatest);
		assert(!genre7.getChildren().contains(greatest));
	}
	
	@Test
	public void testDuplicateGenres() {
		Genre daler = new Genre( "daler" );
		boolean caught;

		try{
			Genre daler2 = new Genre( "daler" );
		}
		catch( IllegalArgumentException e ) {
			return;
		}
		fail();
	}
	
	@Test
	public void addNonTopLevelGenre() {
		Genre blah1 = new Genre( "blah1" );
		Genre blah2 = new Genre( "blah2" );
		blah1.addToGenre( blah2);

		try{
			catalogue.addToCatalogue(blah2);
		}
		catch( IllegalArgumentException e ) {
			return;
		}
		fail();
	}
	
    @After
    public void tearDown() throws Exception {
        testalbum.removeFromGenre();  
    }
}
