package ca.ubc.ece.eece210.mp2.tests;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Test;

import ca.ubc.ece.eece210.mp2.Catalogue;

public class TestRecreateLibraryFromFile {
	@Test
	public void test() {
		Catalogue blah = Catalogue.recreateLibraryFromFile("catalogue.txt");
		blah.saveCatalogueToFile( "rebuild.txt" );
		
		FileInputStream data = null;
		FileInputStream reb = null;
		//Attempt to open the input file where catalogue is saved for reading
		try
		{
			data = new FileInputStream( "catalogue.txt" );
		}
		catch( FileNotFoundException e )
		{
			System.out.println( "Error: input file catalogue.txt not found");
		}
		Scanner input = new Scanner( data );
		input.useDelimiter("\\Z");
		StringBuilder str = new StringBuilder();
		while( input.hasNext() )
		{
			str.append(input.next());
		}
		input.close();
		
		try
		{
			reb = new FileInputStream( "rebuild.txt" );
		}
		catch( FileNotFoundException e )
		{
			System.out.println( "Error: input file rebuild.txt not found");
		}
		Scanner inputr = new Scanner( reb );
		inputr.useDelimiter("\\Z");
		StringBuilder strreb = new StringBuilder();
		while( inputr.hasNext() )
		{
			strreb.append(inputr.next());
		}
		inputr.close();
		assertEquals(str.toString(),strreb.toString());
	}

	}