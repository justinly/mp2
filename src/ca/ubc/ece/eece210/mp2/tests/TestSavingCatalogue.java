package ca.ubc.ece.eece210.mp2.tests;
import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Test;

import ca.ubc.ece.eece210.mp2.*;

/* Note: The catalog had to be tested in a separate file as the check for duplicate genres throws an exception
 * (as the Unclassified genre is instantiated when a category is.) */

public class TestSavingCatalogue {
	Catalogue blah = new Catalogue();
	
	
	@Before
	public void setUp() {
		Genre rock = new Genre( "Rock" );
		Genre Tunak = new Genre( "Tunak" );
		Album greatest = new Album( "Greatest Hits", "Daler Mehndi", new ArrayList<String>(Arrays.asList( "Tunak","Tunak","Tun" )));
		Tunak.addToGenre(greatest);
		
		Album rockAlbum = new Album( "rock album", "rock guy", new ArrayList<String>(Arrays.asList( "song1", "song2", "song3", "song4" )));
		rock.addToGenre( rockAlbum );
		
		Genre classicRock = new Genre( "Classic Rock" );
		Genre hardRock = new Genre( "Hard Shit" );
		Genre synthRock = new Genre( "Synth-Rock" );
		rock.addToGenre( hardRock );
		rock.addToGenre( classicRock );
		rock.addToGenre( synthRock );
		
		Genre punkRock = new Genre( "Punk Rock" );
		hardRock.addToGenre( punkRock );
		
		Album hardAlbum = new Album( "hard album", "hard guy", new ArrayList<String>(Arrays.asList( "song4", "song5", "song7", "song8" )));
		hardRock.addToGenre( hardAlbum );
		
		Genre emo = new Genre( "Emo" );
		Genre trash = new Genre( "Trash" );
		punkRock.addToGenre( emo );
		punkRock.addToGenre( trash );
		
		Album emoAlbum = new Album( "emo album", "emo guy", new ArrayList<String>(Arrays.asList( "song3", "song8", "song0", "songblah" )));
		emo.addToGenre( emoAlbum );
		Album badStuff = new Album( "bad album", "bad guy", new ArrayList<String>(Arrays.asList( "badsong", "song2", "song3", "song4" )));
		emo.addToGenre( badStuff );
		
		Genre epic = new Genre( "Epic" );
		classicRock.addToGenre( epic );
		
		Genre electroRock = new Genre( "electroRock" );
		synthRock.addToGenre( electroRock );
		
		blah.addToCatalogue( rock );
		blah.addToCatalogue( Tunak );  
		
		
	}
	
	@Test
	public void test() {
		blah.saveCatalogueToFile( "catalogue.txt" );
		FileInputStream data = null;
		
		//Attempt to open the input file where catalogue is saved for reading
		try
		{
			data = new FileInputStream( "catalogue.txt" );
		}
		catch( FileNotFoundException e )
		{
			System.out.println( "Error: input file catalogue.txt not found");
		}
		Scanner input = new Scanner( data );
		input.useDelimiter("\\Z");
		StringBuilder str = new StringBuilder();
		while( input.hasNext() )
		{
			str.append(input.next());
		}
		input.close();
		String represent = " {\"genre\":\"Unclassified\"\\End\\ {\"genre\":\"Rock\" < \"album\":\"{\"title\":\"rock_album\",\"performer\":\"rock_guy\",\"songlist\":{0:\"song1\",1:\"song2\",2:\"song3\",3:\"song4\"}}  {\"genre\":\"Hard_Shit\" <  {\"genre\":\"Punk_Rock\" <  {\"genre\":\"Emo\" < \"album\":\"{\"title\":\"emo_album\",\"performer\":\"emo_guy\",\"songlist\":{0:\"song3\",1:\"song8\",2:\"song0\",3:\"songblah\"}} \"album\":\"{\"title\":\"bad_album\",\"performer\":\"bad_guy\",\"songlist\":{0:\"badsong\",1:\"song2\",2:\"song3\",3:\"song4\"}}  >  {\"genre\":\"Trash\" > \"album\":\"{\"title\":\"hard_album\",\"performer\":\"hard_guy\",\"songlist\":{0:\"song4\",1:\"song5\",2:\"song7\",3:\"song8\"}}  >  {\"genre\":\"Classic_Rock\" <  {\"genre\":\"Epic\" >  {\"genre\":\"Synth-Rock\" <  {\"genre\":\"electroRock\" >  > \\End\\ {\"genre\":\"Tunak\" < \"album\":\"{\"title\":\"Greatest_Hits\",\"performer\":\"Daler_Mehndi\",\"songlist\":{0:\"Tunak\",1:\"Tunak\",2:\"Tun\"}}  > \\End\\";
		assertEquals(represent,str.toString());
	}
	

}
