package ca.ubc.ece.eece210.mp2;

import java.util.ArrayList;
import java.util.regex.*;

/**
 * 
 * @author Sathish Gopalakrishnan
 * 
 * This class contains the information needed to represent 
 * an album in our application. 
 * 
 */

public final class Album extends Element {
	private String title;
	private String performer;
	private ArrayList<String> songlist = new ArrayList<String>();
	private Genre genre;
	
	/**
	 * Builds an album with the given title, performer and song list
	 * @requires Requires that all datafields of album ARE NOT symbols used in mark-up (" < ", " >", "\End\", " "album" ", " 
	 * " "genre" ", ""title"", ""performer"", ""songlist""
	 * @param title
	 *            the title of the album
	 * @param author
	 *            the performer 
	 * @param songlist
	 * 			  the list of songs in the album
	 */
	public Album(String title, String performer, ArrayList<String> songlist) {
		this.title = title;
		this.performer = performer;
		this.songlist = songlist;
		this.genre = Catalogue.allGenres.get( "Unclassified" ); //Initializes a new album's genre as "Unclassified"
		genre.addToGenre(this); //Adds this album to the "Unclassified" genre
	}

	/**
	 * Builds an album from the string representation of the object. It is used
	 * when restoring an album from a file.
	 * @param stringRepresentation
	 *            the string representation - Requires that all datafields of album ARE NOT symbols used in mark-up (" < ", " >", "\End\", " "album" ", " 
	 * " "genre" ", ""title"", ""performer"", ""songlist""
	 */
	public Album(String stringRepresentation) {
		String songstring;
		String songs[];
		
		//The text mark-up patterns for each datafield in the Album. 
		Pattern titlePattern = Pattern.compile("\"title\":\"(.*?)\"");
		Pattern performerPattern = Pattern.compile("\"performer\":\"(.*?)\"");
		Pattern songPattern = Pattern.compile("\"songlist\":\\{(.*?)\\}");
		Pattern trackPattern = Pattern.compile("\"(.*?)\"");
		
		//Find match for title
		Matcher titleMatcher = titlePattern.matcher(stringRepresentation);
		if (titleMatcher.find()) {
			title = titleMatcher.group(1);
		}
		//Find match for performer
		Matcher performerMatcher = performerPattern.matcher(stringRepresentation);
		if (performerMatcher.find()) {
			performer = performerMatcher.group(1);
		}
		//Find match for songlist
		Matcher songMatcher = songPattern.matcher(stringRepresentation);
		if (songMatcher.find()) {
			songstring = songMatcher.group(1);
			songs = songstring.split(",");
			
			//Adds tracks to songlist
			for (int i=0; i<songs.length; i++) {
				Matcher trackMatcher = trackPattern.matcher(songs[i]);
				if (trackMatcher.find()) {
					songlist.add(trackMatcher.group(1));
				}
			}
		}
		genre = Catalogue.allGenres.get( "Unclassified" ); //Initializes album's genre to unclassified
	}

	/**
	 * Returns the string representation of the given album. The representation
	 * contains the title, performer and songlist, as well as all the genre
	 * that the book belongs to.
	 * 
	 * @return the string representation
	 */
	public String toString() {
		StringBuilder toJSON = new StringBuilder();
		
		toJSON.append(" \"album\":\"" ); //Identifies entire group as an album object
		toJSON.append("{"); //opening brace of JSON
		toJSON.append("\"title\":\"").append(title).append("\","); //title
		toJSON.append("\"performer\":\"").append(performer).append("\","); //performer
		toJSON.append("\"songlist\":{");
		
		for ( int i = 0; i < songlist.size(); i++ ) { //iterator for songlist
			toJSON.append(i).append(":\"").append( songlist.get(i));
			if (i<songlist.size()-1) {
				toJSON.append("\",");
			}
			else {
				toJSON.append("\"");
			}
			
		}
		toJSON.append("}}");
		String finalStr = toJSON.toString().trim().replaceAll( " ", "_"); //Remove spaces temporarily so they won't interfere with parser
		return ( finalStr + " " );
	}

	/**
	 * Add the book to the given genre
	 * 
	 * @param genre - Requires that genre is not null
	 *            the genre to add the album to.
	 * @modifies - the genre you are adding the album to by adding the album to the genre.
	 * @modifies - Modifies the "genre" datafield in the album to match the passed genre
	 *
	 */
	public void addToGenre(Genre genre) {
		// TODO implement this
		//if (this.genre != null) {
			this.genre.children.remove( this ); //Removes this album from its current parent genre
		//}
		
		this.genre = genre;
		genre.addChild( this ); //Adds this album to genre
	}
	
	/**
	 * Removes the album from its current referenced genre and sets album's "genre" to "Unclassified"
	 * @effects - Modifies the genre referenced by the album  by removing this album from it. Sets this album's genre reference to "Unclassified".
	 */
	public void removeFromGenre()
	{
		genre.children.remove( this );
		genre = Catalogue.allGenres.get("Unclassified");
	}

	/**
	 * Returns the genre that this album belongs to.
	 * 
	 * @return the genre that this album belongs to
	 */
	public Genre getGenre() {
		// TODO implement this
		return genre;
	}

	/**
	 * Returns the title of the album
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Returns the performer of the album
	 * 
	 * @return the performer
	 */
	public String getPerformer() {
		return performer;
	}

	/**
	 * An album cannot have any children (it cannot contain anything).
	 */
	@Override
	public boolean hasChildren() {
		return false;
	}
}