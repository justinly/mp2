package ca.ubc.ece.eece210.mp2;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Container class for all the albums and genres. Its main 
 * responsibility is to save and restore the collection from a file.
 * 
 * @author Sathish Gopalakrishnan
 * 
 */
public final class Catalogue {
	private List<Element> topGenres = new ArrayList<Element>(); // List that stores all the top-level genres (genres with no parent)
	public static Map<String, Genre> allGenres = new HashMap<String, Genre>(); //Map of all genres created with their name as the key
	
	/**
	 * Builds a new, empty catalogue. Creates the default genre "Unclassified" and adds it to its list of top genres
	 * @effects Creates new, empty catalogue and creates/adds the "Unclassified" genre to its list of top-level genres
	 */
	public Catalogue() {
		Genre unclassified = new Genre( "Unclassified" );//Create unclassified genre for all newly created albums to inhabit initially
		topGenres.add( unclassified ); 
		Catalogue.allGenres.put( "Unclassified", unclassified ); //Adds unclassified genre to map of all existing genres
	}

	/**
	 * Builds a new catalogue and restores its contents from the 
	 * given file.
	 * 
	 * @param fileName
	 *            the file from where to restore the library.
	 */
	public Catalogue(String fileName) {
		FileInputStream data = null;
		
		//Attempt to open the input file where catalogue is saved for reading
		try
		{
			data = new FileInputStream( fileName );
		}
		catch( FileNotFoundException e )
		{
			System.out.println( "Error: input file " + fileName + " not found");
		}
		
		Pattern genreTree = Pattern.compile("\\\\End\\\\" );
		Scanner input = new Scanner( data );
		input.useDelimiter( genreTree );
		
		while( input.hasNext() ) //Parse through entire txt file
		{
			//Read next genre hierarchy (a genre and all of its children and their children, etc)
			String next = input.next( );
			//Passes genre hierarchy to restoreCollection and adds the genre it returns to the catalogue
			addToCatalogue( Genre.restoreCollection( next.trim() ) ); 
		}
		input.close();
	}

	/**
	 * Saved the contents of the catalogue to the given file.
	 * @param fileName the file where to save the library
	 * @throws FileNotFoundException 
	 */
	public void saveCatalogueToFile(String fileName)  {
		// TODO implement
		DataOutputStream output = null;
		
		//Opening the output file to write catalogue to
		try
		{
		output =  new DataOutputStream( new FileOutputStream( fileName )) ;
		}
		catch( FileNotFoundException e )
		{
			System.out.println( "Error: " + fileName + " could not be opened");
		}
		
		//Writing all top-level genres and their entire hierarchies to the file
		try
		{
			for( Element element : topGenres ) // Uses top-level genre's toString to write its whole hierarchy to the file
			{
				output.writeBytes( element.toString() );
			}
		//writeElements( topGenres, output );
		}
		catch( IOException e )
		{
			System.out.println( "Error: Could not write to file" );
		}
	}
	
	/**
	 * Adds a top-level genre to the catalogue. 
	 * @params topGenre - non-null top-level genre to be added to the catalogue. 
	 * @effects Adds the top-level "topGenre" to the catalogue.
	 * @throws IllegalArgumentException - If "topGenre" is not a top-level genre (ie. has parents)
	 */
	public void addToCatalogue( Genre topGenre ) throws IllegalArgumentException
	{
		if( Genre.findParent( topGenre) != null )
		{
			throw new IllegalArgumentException();
		}
		else
		{
			topGenres.add( topGenre );
		}
	}
	
	/**
	 * Builds a new catalogue and restores its contents from the 
	 * given file.
	 * 
	 * @param fileName
	 *            the file from where to restore the library.
	 */
	public static Catalogue recreateLibraryFromFile(String fileName){
		return new Catalogue(fileName);
	}
}