package ca.ubc.ece.eece210.mp2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Represents a genre (or collection of albums/genres).
 * 
 * @author Sathish Gopalakrishnan
 * 
 */
public final class Genre extends Element {

	/**
	 * Creates a new genre with the given name.
	 * 
	 * @param name
	 *            the name of the genre. - Requires that "name" is not one of symbols used in markup (" < ", " >", "\End\", ""album"" 
	 *             genre" ", ""title"", ""performer"", ""songlist""
	 * @throws IllegalArgumentException - If "name" matches an existing genre
	 * @effects Creates a genre of the given name and adds it to allGenres, the list of all existing genres.
	 */
	public Genre(String name) throws IllegalArgumentException {
		if(Catalogue.allGenres.containsKey(name))
		{
			System.out.println( "Error: Cannot create a duplicate genre" );
			throw new IllegalArgumentException();
		}
		else
		{
		this.name = name;
		Catalogue.allGenres.put(name, this); //Add new genre to the map of all existing genres
		}
	}

	/**
	 * Restores a genre from its given string representation.
	 * 
	 * @param stringRepresentation - Requires that genres and albums ARE NOT one of symbols used in markup (" < ", " >", "\End\", 
	 * ""album"" genre" ", ""title"", ""performer"", ""songlist""
	 * @effects Creates top-level genre and all subgenres/albums in top-level genre's hierarchy, and adds all children to the correct
	 * parent genre as specified in the input file.
	 */
	public static Genre restoreCollection(String stringRepresentation) {
		// Patterns for genres and albums to help parse through stringRepresentation
		Pattern genrePattern = Pattern.compile("\"genre\":\"(.*?)\"");
		Pattern albumPattern = Pattern.compile("\"album\":\"(.*?)\"" );
		
		Genre currentGenre = null; //Last genre created. Need in case it has children
		Stack<Genre> superGenres = new Stack<Genre>(); //Contains current super genre on top of stack so we can add its children to it
		Scanner input = new Scanner( stringRepresentation ); //Scanner to parse through genre hierarchy string
		
		//These 6 lines are used to find and create the top-level genre in this hierarchy and set up the stack for further child adding
		String next = input.next();
		Matcher genreMatcher = genrePattern.matcher(next); 
		genreMatcher.find(); 
		Genre topGenre = new Genre( genreMatcher.group(1).replaceAll( "_", " " ) ); //Creates the top level genre in hierarchy. Know first token is the top-level genre
		superGenres.push( topGenre ); //Add top genre to top of stack 
		currentGenre = topGenre; //Initializes currentGenre to be the top-level genre
		
		while( input.hasNext() ) //Parse through entire txt file and rebuild hierarchy of this genre group
		{	
			 next = input.next( );
			//Matchers to see if next token matches the album or genre pattern. 
			Matcher albumMatcher = albumPattern.matcher( next );
			genreMatcher = genrePattern.matcher( next );
	
			if( next.equals( "<")) 
			{
				superGenres.push( currentGenre ); //Know we have subgenres/albums since " < " so push currentGenre to stack
			}
			else if( next.equals( ">" ) )
			{
				superGenres.pop(); //Bracket is closed so we can remove the genre on top of the stack (it has no more children)
			}
			else if( genreMatcher.find() ) //We're dealing with a subgenre so must add to genre on top of stack
			{
				currentGenre = new Genre( genreMatcher.group(1).replaceAll( "_", " ") ); //Creates new genre and updates currentGenre's value to this one
				superGenres.peek().addToGenre( currentGenre ); //Adds the currentGenre to the genre on top of stack 
			}
			else if(  albumMatcher.find() )
			{
				superGenres.peek().addToGenre( new Album( next.replaceAll( "_", " ") ) ); //Uses album's string representation constructor
			}
		} 
		
		return topGenre;
	}

	/**
	 * Returns the string representation of a genre
	 * 
	 * @return String representing the genre and all of its descendants (everything in the hierarchy in which it is the root)
	 */
	public String toString() {
		// TODO implement
		StringBuilder toJSON = new StringBuilder();
		toJSON.append("{\"genre\":\"").append(name).append("\""); //genre name
		String finalStr = toJSON.toString().trim().replaceAll( " ", "_"); //Remove spaces temporarily so they won't interfere with parser
		
		//Adds string representations of this genre's children to the string
		if( !children.isEmpty() )
			{
				finalStr += ( " < "); //Encapsulates the children of this genre so we know where to add them during catalogue rebuild
				for( Element child : children ) //Calls toString on all of this genre's children
				{
					finalStr += child.toString();
				}
				finalStr += ( " > " );
			}
		
		if( Genre.findParent( this ) == null ) //Checks if this genre is a top-level genre
		{
			finalStr += "\\End\\"; //Adds top-level tags to enclose genre's entire hierarchy 
		}
		 return (" " + finalStr  );
	}

	/**
	 * Adds the given album or genre to this genre
	 * 
	 * @param b - the element to be added to the collection. Requires that b is not null
	 *            
	 */ //TODO: Remove album or genre from its old super genre before adding
	public void addToGenre(Element b) {
		//Checks if b is a genre and has a parent, if so, removes b from its old parent
		if( b.hasChildren() && findParent( (Genre) b) != null)
		{
			findParent( (Genre) b ).children.remove( b );
			addChild(b);
		}
		else if( !b.hasChildren() ) //Is an album
		{
			( (Album) b).addToGenre( this ); //Uses album's addToGenre which adds it to this genre and
		}
		else //Genre to be added has no parent
		{
			addChild(b);
		}
		
	}
	
	/**
	 * Removes the given album or genre from this genre
	 * @param b - the element to be removed from this genre
	 * 
	 */ 
	public void removeFromGenre( Element b )
	{
		children.remove( b );
	}

	/**
	 * Returns true, since a genre can contain other albums and/or
	 * genres.
	 */
	@Override
	public boolean hasChildren() {
		return true;
	}
	
	/**
	 * Finds the parent of a genre and returns it.
	 * @param subGenre - the genre whose parent you are searching for
	 * @return 	- the parent genre of "subGenre" if it exists
	 * 			- Returns null if "subGenre" has no parent
	 */
	public static Genre findParent( Genre genre )
	{
		for( Genre element : Catalogue.allGenres.values() ) //Turns map into list of all existing genres
		{
			if( element.children.contains( genre ) ) //If an existing genre has "genre" in its children then return it
				return element;
		}
		return null;
	}
}