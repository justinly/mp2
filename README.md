Machine Problem 2: Music Catalogue 2.10
===

### Hello.
Please see the test cases in src/ca/ubc/ece/eece210/tests.
It was necessary to separate out the tests involving categories as we have implemented a feature that checks for duplicate genres. As the category constructor also instantiates a Unclassified genre, this causes our feature to throw an exception.

**### IMPORTANT: PLEASE READ** 
I recently found a small (but possibly bad style/design) error in our code for MP2. There was an instance variable that I forgot to change to "private" instead of "public" and in one of the tests it directly accessed the variable instead of using the public method I made. I made the changes (changed 3 tiny lines of code) and pushed it to BitBucket, but I know that the deadline for MP2 has already passed.

****I was wondering if it would be possible for you to mark the final version of our code, where we made the small edit, even though it's past the deadline. Thank you.******

### Why the strange syntax for saving/restoring from file?
We started with JSON and the regexes proved to be fairly impossible as JSON is actually not that regular (see edwin branch if you would like to see how far we went in that direction before we just went with stuff we could catch for sure.)